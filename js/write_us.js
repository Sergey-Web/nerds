var link = document.querySelector(".btn_map");
var popup = document.querySelector(".write_us");
var cancel = document.querySelector(".btn_cancel");

link.addEventListener("click", function(event){
    event.preventDefault();
    popup.classList.add("write_us_show");
});

cancel.addEventListener("click", function(event){
    event.preventDefault();
    popup.classList.remove("write_us_show");
});


window.addEventListener("keydown", function(event){
    if(event.keyCode == 27){
        if(popup.classList.contains("write_us_show")) {
            popup.classList.remove("write_us_show");
        }
    }

});