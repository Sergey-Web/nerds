function initialize() {
    var mapOptions = {
        zoom: 17,
        center: new google.maps.LatLng(45.039474, 38.969090)
    }
    var map = new google.maps.Map(document.getElementById('map'),
        mapOptions);

    var image = '../img/map_logo.png';
    var myLatLng = new google.maps.LatLng(45.039018, 38.972208);
    var beachMarker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        icon: image
    });
}

google.maps.event.addDomListener(window, 'load', initialize);
